const mongoose = require('mongoose');

const djSetSchema = mongoose.Schema({
  name: { type: String, require: true },
  artwork: { type: String, require: false},
  volumes: [{
    name: { type: String, require: true },
    url: { type: String, require: true },
    publicationDate: Date,
    playlist: [{
      title: { type: String, require: true }
    }]
  }]
});

module.exports = mongoose.model('DjSet', djSetSchema);
