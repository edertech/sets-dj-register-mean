const express = require('express');
const router = express.Router();
const DjSetController = require('../controllers/djset.controller');
const checkAuth = require('../middleware/check-auth');

router.get('', checkAuth, DjSetController.getDjSet);

router.get('/all', DjSetController.getExtDjSet);

router.post('', checkAuth, DjSetController.createDjSet);

router.put('', checkAuth, DjSetController.updateDjSet);

router.delete('/:id', checkAuth, DjSetController.removeDjSet);

module.exports = router;
