const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const djsetRoutes = require('./routes/djset.routes');
const userRoutes = require('./routes/user.routes');

const app = express();

mongoose.connect('mongodb://localhost:27017/sets-dj-db')
  .then(() => console.log('Connected to database!'))
  .catch((error) => console.log(error));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  );
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, PATH, DELETE, PUT, OPTIONS'
  );
  next();
});

app.use('/api/djsets', djsetRoutes);
app.use('/api/user', userRoutes);

module.exports = app;
