const UserController = require('../controllers/djset.controller');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/user');

exports.login = (req, res, next) => {
  let fetchedUser;

  User.findOne({ login: req.body.login }).then(user => {
    if (user === null) {
        return res.status(401).json({
          message: 'User not found!'
        });
    }
    fetchedUser = user;
    if (bcrypt.compareSync(req.body.password, user.password)) {
      const token = jwt.sign({
        login: fetchedUser.login,
        userId: fetchedUser._id
      }, process.env.SET_DJ_HASH,
        { expiresIn: '1h' }
      );
      res.status(200).json({
        token: token,
        expiresIn: 3600
      });
    } else {
      return res.status(401).json({
        message: 'Auth failed!'
      });
    }
  });
};
