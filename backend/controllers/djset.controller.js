const DjSet = require('../models/dj-set');

function sortSets(volumes) {
  volumes.sort((a, b) => {
    if (a.name < b.name) {
      return -1;
    }
    if (a.name > b.name) {
      return 1;
    }
    return 0;
  }).reverse();
}

function parseJson(res, docs) {
  return res.status(200).json({
    message: 'DjSets fetched successfully!',
    content: docs
  })
}

exports.getDjSet = (req, res) => {
  DjSet.find().then(docs => {
    docs.forEach((set) => sortSets(set.volumes));
    return parseJson(res, docs);
  });
};

exports.getExtDjSet = (req, res) => {
  DjSet.find().then(docs => {
    docs.forEach((set) => {
      sortSets(set.volumes);
      set.volumes.forEach((v) => v.playlist=null);
    });
    return parseJson(res, docs);
  });
};

exports.createDjSet = (req, res) => {
  const djset = new DjSet(req.body);

  djset.save().then(createdDjSet =>
    res.status(201).json({
      message: 'DjSet added sucessfully!',
      id: createdDjSet._id
    })
  );
};

exports.updateDjSet = (req, res) => {
  const id = req.body.id;

  DjSet.findById(id).then((model) => {
    const data = req.body;
    model.name = data.name;
    model.artwork = data.artwork;
    model.volumes = data.volumes;
    model.save().then(() =>
      res.status(200).json({
        message: 'DjSet updated sucessfully!'
      })
    );
  });
};

exports.removeDjSet = (req, res) => {
  const id = req.params.id;
  DjSet.deleteOne({ _id: id }).then((result) =>
    res.status(200).json({ message: 'DjSet deleted!' })
  );
};
