import { NgModule } from "@angular/core";
import { AppMaterialModule } from "./app-material.module";
import { FormsModule } from '@angular/forms';
import { LoginComponent } from "./auth/login/login.component";
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './auth/auth-interceptor';
import { ErrorInterceptor } from './error-interceptor';
import { CommonModule } from "@angular/common";

@NgModule({
    declarations: [
        LoginComponent
    ],
    imports: [
        AppMaterialModule,
        HttpClientModule,
        FormsModule,
        CommonModule,
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
    ]
})
export class AuthModule {

}
