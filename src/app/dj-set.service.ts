import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";
import { map } from 'rxjs/operators';
import { environment } from "src/environments/environment";
import { DjSet, DjSetVolume, Track } from "./dj-set.model";

const BE_URL = environment.urlApi + '/djsets';

@Injectable({ providedIn: 'root' })
export class DjSetService {
    private djsets: DjSet[] = [];

    private djSetsUpdated = new Subject<DjSet[]>();

    constructor(private http: HttpClient) {
        this.http
            .get<{ message: string, content: any[] }>(BE_URL)
            .pipe(map((djSetData) => {
                return djSetData.content.map(djset => {
                    let volumes = djset.volumes as any[];
                    let volumesParsed = [] as DjSetVolume[];
                    volumes.forEach(v => {
                        let playlist = v.playlist as any[];
                        let playlistParsed = [] as Track[];
                        playlist.forEach(t =>
                            playlistParsed.push({
                                id: t._id,
                                title: t.title
                            } as Track)
                        );
                        volumesParsed.push({
                            id: v._id,
                            name: v.name,
                            artwork: v.artwork,
                            url: v.url,
                            publicationDate: v.publicationDate,
                            playlist: playlistParsed
                        } as DjSetVolume)
                    });
                    let d = {
                        name: djset.name,
                        id: djset._id,
                        artwork: djset.artwork,
                        volumes: volumesParsed
                    } as DjSet;

                    return d;
                });
            }))
            .subscribe(djSetTransformed => {
                this.djsets = djSetTransformed;
                this.djSetsUpdated.next([...this.djsets]);
            });
    }

    getDjSets(): DjSet[] {
        return [...this.djsets];
    }

    getDjSetsUpdateListener(): Observable<DjSet[]> {
        return this.djSetsUpdated.asObservable();
    }

    addDjSet(name: string, artwork: string) {
        const djset: DjSet = {
            name: name,
            artwork: artwork
        };
        this.http.post<{ message: string, id: string }>(BE_URL, djset)
            .subscribe((data) => {
                djset.id = data.id;
                this.djsets.push(djset);
                this.djSetsUpdated.next([...this.djsets]);
            });
    }


    addVolume(name: string, url: string, publicationDate: any, djset: DjSet) {
        if (!djset) {
            return;
        }
        this.djsets.forEach(t => {
            if (t.id === djset.id) {
                if (!t.volumes) {
                    t.volumes = [];
                }
                let newId = t.volumes.length + 1;
                let volume: DjSetVolume = {
                    name: 'Vol. ' + this.pad(newId, 3) + ' (' + name + ')',
                    url: url,
                    publicationDate: publicationDate,
                    playlist: []
                };
                t.volumes.push(volume);
                this.updateAll(djset);
            }
        });
    }

    updateAll(djset: DjSet) {
        this.http.put<{ message: string }>(BE_URL, djset)
            .subscribe((data) => {
                console.log('put executed! ', data)
            });
    }

    addTrack(title: string, volume: DjSetVolume) {
        let djset = null;
        this.djsets.forEach(d =>
            d.volumes?.forEach(v => {
                if (v.id === volume.id) {
                    djset = d;
                    return;
                }
            })
        );

        if (djset) {
            if (!volume.playlist) {
                volume.playlist = [];
            }
            volume.playlist.push({
                title: title 
            });
            this.updateAll(djset);
        }
    }

    private pad(num: number, size: number): string {
        let s = num + "";
        while (s.length < size) s = "0" + s;
        return s;
    }

    deleteDjSet(id?: string) {
        this.http.delete(BE_URL + '/' + id)
            .subscribe(() => {
                const updatedDjSets = this.djsets.filter(t => t.id !== id);
                this.djsets = updatedDjSets;
                this.djSetsUpdated.next([...this.djsets])
            });
    }
}
