import { NgModule } from "@angular/core";
import { DjSetComponent, DjSetDialog, VolumeDialog } from './djset-list/djset.component';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppMaterialModule } from "./app-material.module";

@NgModule({
    declarations: [
        DjSetComponent,
        DjSetDialog,
        VolumeDialog,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        AppMaterialModule
    ]
})
export class DjSetModule {

}
