import { Component, Inject, OnDestroy, ViewChild } from "@angular/core";
import { NgForm } from "@angular/forms";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { MatAccordion } from "@angular/material/expansion";
import { MatListOption, MatSelectionList } from "@angular/material/list";
import { Subscription } from "rxjs";
import { DjSet, DjSetVolume, Track } from "../dj-set.model";
import { DjSetService } from "../dj-set.service";

export interface DjSetDialogData {
  name: string,
  artwork: string,
  type: string,
  canceled: boolean,
}
export interface VolumeDialogData {
  name: string,
  url: string,
  published: boolean,
  playlist: Track[],
  canceled: boolean,
  type: string
}

@Component({
  selector: 'djset-list',
  templateUrl: './djset.component.html',
  styleUrls: ['./djset.component.css']
})
export class DjSetComponent implements OnDestroy {
  @ViewChild(MatAccordion) accordion: MatAccordion = new MatAccordion();
  djsets: DjSet[];

  private djsetsSub: Subscription;

  constructor(public djsetService: DjSetService, public dialogSet: MatDialog) {
    this.djsets = this.djsetService.getDjSets();
    this.djsetsSub = this.djsetService.getDjSetsUpdateListener()
      .subscribe((djsets: DjSet[]) => {
        this.djsets = djsets;
      });
  }

  ngOnDestroy(): void {
    this.djsetsSub.unsubscribe();
  }

  //-------------------- DJ SET Control --------------------

  onCreateSetDialog(): void {
    const dialogRef = this.dialogSet.open(DjSetDialog, {
      width: '50%',
      data: {
        name: '',
        artwork: '',
        type: 'Create'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      const data: DjSetDialogData = result;
      if (data) {
        this.djsetService.addDjSet(data.name, data.artwork);
      }
    });
  }

  onEditSetDialog(djset: DjSet): void {
    const dialogRef = this.dialogSet.open(DjSetDialog, {
      width: '50%',
      data: {
        name: djset.name,
        artwork: djset.artwork,
        type: 'Edit'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      const data: DjSetDialogData = result;
      if (data) {
        djset.name = data.name;
        djset.artwork = data.artwork;
        this.djsetService.updateAll(djset)
      }
    });
  }

  onDeleteSet(id?: string) {
    if (confirm('Are you sure to delete?')) {
      this.djsetService.deleteDjSet(id);
    }
  }

  //-------------------- Volume Control --------------------

  onCreateVolumeDialog(djset: DjSet): void {
    const dialogRef = this.dialogSet.open(VolumeDialog, {
      width: '50%',
      data: {
        name: '',
        url: '',
        published: false,
        type: 'Create',
        canceled: true
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      const data: VolumeDialogData = result;
      if (data && data.canceled === false) {
        let publicationDate = data.published === true ? new Date(): null;
        this.djsetService.addVolume(data.name, data.url, publicationDate, djset);
      }
    });
  }

  onEditVolumeDialog(djset: DjSet, volume: DjSetVolume): void {
    const dialogRef = this.dialogSet.open(VolumeDialog, {
      width: '50%',
      data: {
        name: volume.name,
        url: volume.url,
        playlist: volume.playlist,
        published: volume.publicationDate != null,
        type: 'Edit',
        canceled: true
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      const data: VolumeDialogData = result;
      if (data && data.canceled === false) {
        volume.name = data.name;
        volume.url = data.url;
        volume.playlist = data.playlist;
        if (data.published === true && volume.publicationDate === null) {
          volume.publicationDate = new Date();
        } else if (data.published === false) {
          volume.publicationDate = null;
        }
        this.djsetService.updateAll(djset)
      }
    });
  }

  onDeleteVolume(djset: DjSet, volume: DjSetVolume) {
    if (confirm('Are you sure to delete?')) {
      let index = 0;
      djset.volumes?.forEach(v => {
        if (v.id === volume.id) {
          djset.volumes?.splice(index, 1);
          this.djsetService.updateAll(djset)
        }
        index++;
      });
    }
  }
}


//-------------------- Dialog Components --------------------
@Component({
  selector: 'djset-dialog',
  templateUrl: 'djset.dialog.html'
})
export class DjSetDialog {
  constructor(
    public dialogRef: MatDialogRef<DjSetDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DjSetDialogData
  ) { }

  onClose(): void {
    this.data.canceled = true;
    this.dialogRef.close();
  }

  onSubmit(form: NgForm): void {
    if (form.valid && this.data.name.trim() != '') {
      this.data.canceled = false;
      this.dialogRef.close(this.data);
    }
  }
}

@Component({
  selector: 'volume-dialog',
  templateUrl: 'volume.dialog.html'
})
export class VolumeDialog {
  originalPlaylists: Track[] = [];
  selectedTracks!: MatSelectionList;

  constructor(
    public dialogRef: MatDialogRef<VolumeDialog>,
    @Inject(MAT_DIALOG_DATA) public data: VolumeDialogData
  ) {
    if (data.playlist) {
      this.originalPlaylists = [...data.playlist];
    }
  }

  onClose(): void {
    this.data.canceled = true;
    const pl = this.data.playlist;
    pl.splice(0, pl.length);
    this.originalPlaylists.forEach(t => this.data.playlist.push(t));
    this.dialogRef.close(this.data);
  }

  onSubmit(form: NgForm): void {
    if (form.valid && this.data.name.trim() != '') {
      this.data.canceled = false;
      this.dialogRef.close(this.data);
    }
  }

  onAddTrack(title: HTMLInputElement): void {
    if (title.value.trim() === '') {
      return;
    }
    this.data.playlist?.push({
      title: title.value
    });
    title.value = '';
  }

  onSelectionChange(selection: MatSelectionList): void {
    this.selectedTracks = selection;
  }

  countSelectedTracks(): number {
    if (this.selectedTracks) {
      return this.selectedTracks.selectedOptions.selected.length;
    } else {
      return 0;
    }
  }

  isSelectedTracks(): boolean {
    return this.countSelectedTracks() > 0;
  }

  canMoveTrack(): boolean {
    return this.countSelectedTracks() === 1 && this.data.playlist.length > 1; 
  }

  onDeleteTracks(): void {
    const selected: MatListOption[] = this.selectedTracks.selectedOptions.selected;
    if (confirm('Are you sure to delete?')) {
      let pl = this.data.playlist;
      selected.forEach(opt => {
        const track: Track = opt.value;
        let iTrack = 0;
        pl.forEach(t => {
          if (t.id === track.id) {
            pl.splice(iTrack, 1);
          }
          iTrack++;
        });
      });
    }
  }

  onDownTrack(): void {
    const selected: MatListOption[] = this.selectedTracks.selectedOptions.selected;
    const pl = this.data.playlist;
    const selectedTrack: Track = selected[0].value;
    let plOrdered: Track[] = [];

    for (let i = 0; i < pl.length; i++) {
      const t: Track = pl[i];
      if (t.id === selectedTrack.id && i < pl.length - 1) {
        plOrdered.push(pl[i + 1]);
        i++;
      } {
        plOrdered.push(t);
      }
    }
    if (plOrdered.length > 0) {
      this.data.playlist = plOrdered;
    }
  }

  onUpTrack(): void {
    const selected: MatListOption[] = this.selectedTracks.selectedOptions.selected;
    const pl = this.data.playlist;
    const selectedTrack: Track = selected[0].value;
    let plOrdered: Track[] = [];

    for (let i = 0; i < pl.length; i++) {
      const t: Track = pl[i];
      if (t.id === selectedTrack.id && i > 0) {
        let prior = plOrdered[i - 1];
        plOrdered.splice(i - 1, 1);
        plOrdered.push(t);
        plOrdered.push(prior);
      } else {
        plOrdered.push(t);
      }
    }
    if (plOrdered.length > 0) {
      this.data.playlist = plOrdered;
    }
  }
}
