export interface DjSet {
   id?: string;
   name: string;
   artwork?: string;
   volumes?: DjSetVolume[]
}

export interface DjSetVolume {
   id?: string;
   name: string;
   url: string;
   publicationDate?: Date | null;
   playlist?: Track[]
}

export interface Track {
   id?: string;
   title: string;
}
